# README #

A small iOS game for clearing box with same color.

### What is this repository for? ###

* Playing with swift game development

### How do I get set up? ###

* Clone the code to your local repository
* Open it in xCode
* Build & Run it

### Who do I talk to? ###

* Tom Yu (kangleyu@gmail.com or yukangle@outlook.com)