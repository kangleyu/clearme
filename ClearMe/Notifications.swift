//
//  Notifications.swift
//  ClearMe
//
//  Created by Tom Yu on 7/13/16.
//  Copyright © 2016 kangleyu. All rights reserved.
//

import Foundation

struct ClearMeNotifications {
    static let NewScoreGeneratedNotification = "NewScoreGeneratedNotification"
    static let NewScoreGeneratedNotificationKey = "NewScoreGeneratedNotificationKey"
}
