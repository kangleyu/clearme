//
//  ClearMeView.swift
//  ClearMe
//
//  Created by Tom Yu on 7/10/16.
//  Copyright © 2016 kangleyu. All rights reserved.
//

import UIKit

@IBDesignable
class ClearMeView: UIView, UIDynamicAnimatorDelegate {

    fileprivate let boxPerLine = 10
    fileprivate let removeBoxDelay = 0.15
    fileprivate var isAllCached = false
    
    fileprivate var boxes = [String:BoxView]()
    
    var delegate: ClearMeViewDelegate?;
    
    fileprivate var boxSize: CGSize {
        let size = bounds.size.width / CGFloat(boxPerLine)
        return CGSize(width: size, height: size)
    }
    
    override func draw(_ rect: CGRect) {
        reset()
    }
    
    func boxTapped(_ recognizer: UITapGestureRecognizer) {

        if let box = recognizer.view as? BoxView {
            let index = getIndex(box)
            print(index)
            
            // get all matched boxes
            let matchedIndexes = getAllMatched(startIndex: index, inBoxes: boxes)
            
            let score = matchedIndexes.count * matchedIndexes.count
            if score > 1 {
                // publish the new added score
                let notification = Notification(
                    name: Notification.Name(rawValue: ClearMeNotifications.NewScoreGeneratedNotification),
                    object: self,
                    userInfo: [ClearMeNotifications.NewScoreGeneratedNotificationKey:score])
                NotificationCenter.default.post(notification)
            }

            // remove all of matched boxes
            updateTitleForMatched(matchedIndexes, delay: removeBoxDelay) { [unowned self]
                updated in
                if updated {
                    self.removeAllMatched(matchedIndexes) { [unowned self]
                        removed in
                        if removed {
                            self.moveDown(&self.boxes) {
                                [unowned self]
                                done in
                                if (done) {
                                    self.moveLeft(&self.boxes) { _ in
                                        self.verifyGameState(self.boxes)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    fileprivate func verifyGameState(_ boxes:[String: BoxView]) {
        let hasMatched = match(boxPerLine, 1)
        if !hasMatched {
            delegate?.endGame()
        }
        
    }
    
    fileprivate func match(_ startRow: Int, _ startColumn: Int) -> Bool {
        if isValid(startRow, column: startColumn) {
            if let box = getBoxView(startRow, column: startColumn) {
                if let upbox = getBoxView(startRow - 1, column: startColumn) {
                    if box.backgroundColor == upbox.backgroundColor {
                        return true
                    }
                }
                if let downbox = getBoxView(startRow, column: startColumn + 1) {
                    if box.backgroundColor == downbox.backgroundColor {
                        return true
                    }
                }
            }
            return match(startRow - 1, startColumn) || match(startRow, startColumn + 1);
        }
        
        return false
    }
    
    fileprivate func moveDown(_ boxes: inout [String: BoxView], completed: (_ succeed: Bool) -> ()) {
        for line in 0..<boxPerLine {
            let row = boxPerLine - line
            for column in 1...boxPerLine {
                if let _ = getBoxView(row, column: column) {
                    // do nothing
                } else {
                    moveAboveDown(&boxes, startRow: row, startColumn: column)
                }
            }
        }
        completed(true)
    }
    
    fileprivate func moveLeft(_ boxes: inout [String: BoxView], completed: (_ succeed: Bool) -> ()) {
        var pivot1 = 1
        var pivot2 = 2
        var offset = 0
        
        while pivot1 <= boxPerLine {
            if let _ = getBoxView(boxPerLine, column: pivot1) {
                if offset > 0 {
                    moveColumnToLeft(&boxes, column: pivot1, offset: offset)
                }
                pivot1 = pivot1 + 1
                pivot2 = pivot2 + 1
            } else {
                if pivot2 > boxPerLine {
                    completed(true)
                    return
                }
                offset = offset + 1
                var nextBox = getBoxView(boxPerLine, column: pivot2)
                while nextBox == nil {
                    pivot2 = pivot2 + 1
                    if pivot2 > boxPerLine {
                        completed(true)
                        return
                    }
                    offset = offset + 1
                    nextBox = getBoxView(boxPerLine, column: pivot2)
                }
                moveColumnToLeft(&boxes, column: pivot2, offset: offset)
                pivot1 = pivot2 + 1
                pivot2 = pivot1 + 1
            }
        }
        completed(true)
    }
    
    fileprivate func moveColumnToLeft(_ boxes: inout [String: BoxView], column: Int, offset: Int) {
        for line in 0..<boxPerLine {
            let row = boxPerLine - line
            if let box = getBoxView(row, column: column) {
                box.center.x = box.center.x - CGFloat(offset) * boxSize.width
                boxes["\(row),\(column - offset)"] = box
                boxes.removeValue(forKey: "\(row),\(column)")
            } else {
                return
            }
        }
    }
    
    fileprivate func moveAboveDown(_ boxes: inout [String: BoxView], startRow: Int, startColumn: Int) {
        if isValid(startRow - 1, column: startColumn) {
            var pivot1 = startRow - 1
            var pivot2 = startRow - 2
            var offset = 1
            
            while pivot1 > 0 {
                if let box = getBoxView(pivot1, column: startColumn) {
                    // move this done
                    box.center.y = box.center.y + CGFloat(offset) * boxSize.height
                    boxes["\(pivot1 + offset),\(startColumn)"] = box
                    boxes.removeValue(forKey: "\(pivot1),\(startColumn)")
                    pivot1 = pivot2
                    pivot2 = pivot1 - 1
                } else {
                    pivot1 = pivot1 - 1
                    pivot2 = pivot1 - 1
                    offset += 1
                }
            }
            
        }
    }
    
    func removeAllMatched(_ indexes: [String], completed: (_ succeed: Bool) -> ()) {
        var isSucceed = false
        let count = indexes.count
        if count > 1 {
            for index in indexes {
                if let box = boxes[index] {
                    box.removeFromSuperview()
                    boxes.removeValue(forKey: index)
                }
            }
            isSucceed = true
        }
        completed(isSucceed)
    }

    func updateTitleForMatched(_ indexes: [String], delay: Double, completed: @escaping (_ succeed: Bool) -> ()) {
        let count = indexes.count
        if count > 1 {
            for index in indexes {
                if let box = boxes[index] {
                    box.backgroundColor = UIColor.backgroundColor
                    let countLabel = UILabel(frame: box.bounds)
                    countLabel.center.x = box.bounds.midX
                    countLabel.center.y = box.bounds.midY
                    countLabel.textAlignment = .center
                    countLabel.font = countLabel.font.withSize(12)
                    countLabel.text = "+\(count)"
                    box.addSubview(countLabel)
                }
            }
            self.delay(delay) {
                completed(true)
            }
        } else {
            self.delay(delay) {
                completed(false)
            }
        }
        
    }
    
    fileprivate func delay(_ delay: Double, closure: @escaping () -> ()) {
        let triggerTime = Int64(delay * Double(NSEC_PER_SEC))
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(triggerTime) / Double(NSEC_PER_SEC), execute: closure)
    }
    
    fileprivate func getAllMatched(startIndex index: (String, String), inBoxes: [String: BoxView]) -> [String] {
        var matched = [String]()
        let startRow = Int(index.0)!
        let startColumn = Int(index.1)!
        matched.append("\(startRow),\(startColumn)")
        if let startBox = inBoxes["\(index.0),\(index.1)"] as BoxView? {
            searchUp(startRow - 1, startColumn: startColumn, startBox: startBox, matched: &matched)
            searchDown(startRow + 1, startColumn: startColumn, startBox: startBox, matched: &matched)
            searchLeft(startRow, startColumn: startColumn - 1, startBox: startBox, matched: &matched)
            searchRight(startRow, startColumn: startColumn + 1, startBox: startBox, matched: &matched)
        }
        
        return matched
    }
    
    fileprivate func searchUp(_ startRow: Int, startColumn: Int, startBox: BoxView, matched: inout [String]) {
        if isValid(startRow, column: startColumn) {
            if let box = getBoxView(startRow, column: startColumn) {
                if startBox.backgroundColor == box.backgroundColor {
                    let index = "\(startRow),\(startColumn)"
                    if !matched.contains(index) {
                        matched.append(index)
                        searchUp(startRow - 1, startColumn: startColumn, startBox: startBox, matched: &matched)
                        searchLeft(startRow, startColumn: startColumn - 1, startBox: startBox, matched: &matched)
                        searchRight(startRow, startColumn: startColumn + 1, startBox: startBox, matched: &matched)
                    }
                    
                } else {
                    return
                }
            }
        }
    }
    
    fileprivate func searchDown(_ startRow: Int, startColumn: Int, startBox: BoxView, matched: inout [String]) {
        if isValid(startRow, column: startColumn) {
            if let box = getBoxView(startRow, column: startColumn) {
                if startBox.backgroundColor == box.backgroundColor {
                    let index = "\(startRow),\(startColumn)"
                    if !matched.contains(index) {
                        matched.append(index)
                        searchDown(startRow + 1, startColumn: startColumn, startBox: startBox, matched: &matched)
                        searchLeft(startRow, startColumn: startColumn - 1, startBox: startBox, matched: &matched)
                        searchRight(startRow, startColumn: startColumn + 1, startBox: startBox, matched: &matched)
                    }
                    
                } else {
                    return
                }
            }
        }
    }
    
    fileprivate func searchLeft(_ startRow: Int, startColumn: Int, startBox: BoxView, matched: inout [String]) {
        if isValid(startRow, column: startColumn) {
            if let box = getBoxView(startRow, column: startColumn) {
                if startBox.backgroundColor == box.backgroundColor {
                    let index = "\(startRow),\(startColumn)"
                    if !matched.contains(index) {
                        matched.append(index)
                        searchLeft(startRow, startColumn: startColumn - 1, startBox: startBox, matched: &matched)
                        searchUp(startRow - 1, startColumn: startColumn, startBox: startBox, matched: &matched)
                        searchDown(startRow + 1, startColumn: startColumn, startBox: startBox, matched: &matched)
                    }
                    
                } else {
                    return
                }
            }
        }
    }
    
    fileprivate func searchRight(_ startRow: Int, startColumn: Int, startBox: BoxView, matched: inout [String]) {
        if isValid(startRow, column: startColumn) {
            if let box = getBoxView(startRow, column: startColumn) {
                if startBox.backgroundColor == box.backgroundColor {
                    let index = "\(startRow),\(startColumn)"
                    if !matched.contains(index) {
                        matched.append(index)
                        searchRight(startRow, startColumn: startColumn + 1, startBox: startBox, matched: &matched)
                        searchUp(startRow - 1, startColumn: startColumn, startBox: startBox, matched: &matched)
                        searchDown(startRow + 1, startColumn: startColumn, startBox: startBox, matched: &matched)
                    }                    
                } else {
                    return
                }
            }
        }
    }
    
    fileprivate func isValid(_ row: Int, column: Int) -> Bool {
        return row > 0 && row <= boxPerLine && column > 0 && column <= boxPerLine
    }

    func getBoxView(_ row: Int, column: Int) -> BoxView? {
        if row <= 0 || row > boxPerLine || column <= 0 || column > boxPerLine {
            return nil
        }
        
        let index = "\(row),\(column)"
        
        if isAllCached {
            if let box = boxes[index] {
                return box
            }
        } else {
            var frame = CGRect(origin: CGPoint.zero, size: boxSize)
            frame.origin.x = (CGFloat(column) - 1) * boxSize.width
            frame.origin.y = (CGFloat(row) - 1) * boxSize.height
            
            frame = frame.insetBy(dx: -0.5, dy: -0.5)
            let box = BoxView(frame: frame)
            boxes[index] = box
            return box
        }
        return nil
    }
    
    func getIndex(_ box: UIView) -> (row:String, column:String) {
        let row = box.frame.origin.y / boxSize.height + 1
        let column = box.frame.origin.x / boxSize.width + 1
        
        return ("\(lroundf(Float(row)))", "\(lroundf(Float(column)))")
    }
    
    func reset() {
        if boxes.count > 0 {
            boxes.forEach { (map) in
                map.1.removeFromSuperview()
            }
            boxes.removeAll()
        }
        
        isAllCached = false
        if !isAllCached {
            for row in 1...boxPerLine {
                for column in 1...boxPerLine {
                    if let box = getBoxView(row, column: column) {
                        box.layer.borderColor = UIColor.borderColor.cgColor;
                        box.layer.borderWidth = 1;
                        box.backgroundColor = UIColor.random
                        
                        box.addGestureRecognizer(UITapGestureRecognizer(target:self, action: #selector(boxTapped(_:))))
                        
                        addSubview(box)
                    }
                }
            }
            isAllCached = true
        }
    }
}
