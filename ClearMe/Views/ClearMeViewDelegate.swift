//
//  ClearMeViewDelegate.swift
//  ClearMe
//
//  Created by Tom Yu on 7/16/16.
//  Copyright © 2016 kangleyu. All rights reserved.
//

import Foundation

protocol ClearMeViewDelegate {
    func endGame()
}