//
//  ViewController.swift
//  ClearMe
//
//  Created by Tom Yu on 7/10/16.
//  Copyright © 2016 kangleyu. All rights reserved.
//

import UIKit

class ClearMeViewController: UIViewController, ClearMeViewDelegate {
    
    // MARK: Declarations
    fileprivate let colorBoxSide = 64
    
    // MARK: Lifecycle
    
    // MARK: View Lifecycle
    
    // MARK: Layout

    // MARK: UI Interaction
    
    @IBOutlet var mainView: UIView! {
        didSet {
            mainView.backgroundColor = UIColor.backgroundColor
        }
    }
    
    @IBOutlet weak var recordLabel: UILabel!

    @IBOutlet weak var scoreLabel: UILabel!
    
    @IBOutlet weak var gameView: ClearMeView! {
        didSet {
            gameView.backgroundColor = UIColor.backgroundColor
            gameView.delegate = self
            NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: ClearMeNotifications.NewScoreGeneratedNotification), object: nil, queue: OperationQueue.main) { (notification) in
                if let score = (notification as NSNotification).userInfo?[ClearMeNotifications.NewScoreGeneratedNotificationKey] as? Int {
                    if let currentScore = self.score {
                        self.score = currentScore + score
                    } else {
                        self.score = score
                    }
                }
            }
        }
    }
    
    
    @IBAction func restart() {
        score = 0
        gameView.reset()
    }
    
    // MARK: Properties
    fileprivate var score: Int? {
        get {
            if let value = Int(scoreLabel.text!) {
                return Int(value)
            } else {
                return nil
            }
        }
        set {
            scoreLabel.text = String(newValue!)
        }
    }
    
    // MARK: Delegate
    
    func endGame() {
        let alert = UIAlertController(title: "Game Over", message: "Message", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Restart", style: UIAlertActionStyle.default, handler: { [unowned self] _ in self.restart() }))
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: Helpers
    
}

