//
//  UIColorExtension.swift
//  ClearMe
//
//  Created by Tom Yu on 7/10/16.
//  Copyright © 2016 kangleyu. All rights reserved.
//

import UIKit

extension UIColor {
    class var primaryColor: UIColor {
        return UIColor(red: 34/255, green: 167/255, blue: 240/255, alpha: 1.0)
    }
    
    class var secondaryColor: UIColor {
        return UIColor.blue
    }
    
    class var backgroundColor: UIColor {
        return UIColor.pumiceColor()
    }
    
    class var borderColor: UIColor {
        return UIColor(red: 228/255, green: 241/255, blue: 254/255, alpha: 1.0)
    }
    
    class func cabaretColor() -> UIColor {
        return UIColor(red: 210/255, green: 82/255, blue: 127/255, alpha: 1.0)
    }
    
    class func softRedColor() -> UIColor {
        return UIColor(red: 236/255, green: 100/255, blue: 75/255, alpha: 1.0)
    }
    
    class func snuffColor() -> UIColor {
        return UIColor(red: 220/255, green: 198/255, blue: 224/255, alpha: 1.0)
    }
    
    class func sanMarinoColor() -> UIColor {
        return UIColor(red: 68/255, green: 108/255, blue: 179/255, alpha: 1.0)
    }
    
    class func pictionBlueColor() -> UIColor {
        return UIColor(red: 34/255, green: 167/255, blue: 240/255, alpha: 1.0)
    }
    
    class func madisonColor() -> UIColor {
        return UIColor(red: 44/255, green: 62/255, blue: 80/255, alpha: 1.0)
    }
    
    class func jadeColor() -> UIColor {
        return UIColor(red: 0/255, green: 177/255, blue: 106/255, alpha: 1.0)
    }
    
    class func seaBuckthornColor() -> UIColor {
        return UIColor(red: 235/255, green: 151/255, blue: 78/255, alpha: 1.0)
    }
    
    class func pumiceColor() -> UIColor {
        return UIColor(red: 210/255, green: 215/255, blue: 211/255, alpha: 1.0)
    }
    
    class var random: UIColor {
        switch arc4random() % 5 {
        case 0: return UIColor.softRedColor()
        case 1: return UIColor.cabaretColor()
        case 2: return UIColor.seaBuckthornColor()
        case 3: return UIColor.jadeColor()
        case 4: return UIColor.pictionBlueColor()
        default: return UIColor.sanMarinoColor()
        }
    }
}
